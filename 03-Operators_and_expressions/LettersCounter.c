#include <stdio.h>
#include <string.h>
#define LEN_ASCII 256
#define LEN_BUFF 300

int main() {
	int i = 0, j = 0, t = 0, k = 0;
	unsigned char buff[LEN_BUFF];
	unsigned char count[LEN_ASCII] = {0};

	fgets(buff, LEN_BUFF, stdin);

	//считаем количество символов
	while (buff[i] != '\n') {
		count[buff[i++]]++;
	}

	//теперь buff будет хранить индексы, чтобы потом можно было их тоже сортировать
	for (i = 0; i < 256; i++)
		buff[i] = i;

	//сортируем массив count и его индексы buff
	for (i = 1; i < 256; i++)
		for (j = i; j > 0; j--) {
			k = count[j - 1];
			t = buff[j-1];
			if (count[j] > count[j - 1]){
				count[j - 1] = count[j];
				count[j] = k;
				buff[j - 1] = buff[j];
				buff[j] = t;
			}
		}

	// выводим данные на консоль
	for (i = 0; i < 256; i++) {
		if (count[i] != 0 && buff[i] != 32)
			printf("|\t%c\t|\t%d\t|\n", buff[i], count[i]);
	}

	return 0;
}
