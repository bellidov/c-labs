#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 5

int main() {
	int msv[N] = {0};
	int i = 0, j = 0, sum1 = 0, sum2 = 0, start1 = 0, start2 = 0;
	srand(time(0));

	//�������� ������ ���������� �������
	for (i = 0; i < N; i++)
		msv[i] = rand()%198+(-99);
	//����� �� �������
	for (i = 0; i < N; i++)
		printf("%d, ", msv[i]);
	
	//��������
	for (i = 0, j = N-1; j >= i; i++, j--) {
		if (msv[i] < 0)
			start1 = 1;
		if (msv[j] > 0)
			start2 = 1;
		if (start1)
			sum1 += msv[i];
		if (start2 && i != j)
			sum2 += msv[j];
	}

	printf("\n%d\n", sum1 + sum2);
	return 0;
}