/*�������� ���������, ������� ��������� ������������� ������
������� N, � ����� ������� ����� ��������� ����� ����������� 
� ������������ ����������*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20



int main() {
	int Imax = 0, Imin = 0, i, buff, flag = 0;
	int arr[N] = {0};
	srand(time(0));
	for (i = 0; i < N; i++)
		arr[i] = rand() % N + 0;

	//��������� ������� ����������� � ������������ ��������
	buff = arr[0];
	for (i = 1; i < N; i++)
	if (arr[i]>buff){
		buff = arr[i];
		Imax = i;
	}
	for (i = 1; i < N; i++)
	if (arr[i]<buff){
		buff = arr[i];
		Imin = i;
	}

	//���������
	buff = 0;
	i = 0;
	while(i != N) {
		if ((i == Imax || i == Imin) || flag == 1) {
			buff += arr[i];
			flag = 1;
			i++;
		} 	
		if ((i == Imax || i == Imin) && flag == 1) {
			flag = 0;
			buff += arr[i];
			i++;
		}
		if (flag == 0)
			i++;
	}

	//������� �� �������
	printf("You number array is: ");
	for (i = 0; i < N; i++)
		printf("%d ", arr[i]);
	printf("\nImin = %d, Imax = %d\n", Imin, Imax);
	printf("The sum is = %d\n", buff);
	return 0;
}