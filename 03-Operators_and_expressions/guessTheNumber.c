#include <stdio.h>
#include <time.h>

int main() {
	int number = 0, user = 1, count = 0;
	char msg[][19] = { "more!\n", "less!\n", "You guessed it!\n" };

	srand(time(0));
	number = rand() % 101;  // ��� �����, ������� ����� �������

	printf("Guess the number!\n(from 0 to 100) \n");

	while (user != number) {
		if (scanf("%d", &user)) {       // zashita ot durakov
			count++;
			printf("%s", msg[user<number ? 0 : (user>number ? 1 : 2)]);  // ���������, � ����������� �� ���������� �����
		}
		else {
			printf("(Please, enter only integer numbers...)\n"); 
			fflush(stdin);  
			}
	}

	printf("number of attempts: %d\n", count);
	return 0;

}