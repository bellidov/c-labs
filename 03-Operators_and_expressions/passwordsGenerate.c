#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN_PASS 8
#define NUM_PASS 10


void getPasswrd(int x){
	int i = 0;
	char passw[LEN_PASS + 1] = { 0 };

	srand(time(0)+x);  //xxx pozvoliaet meniat nachalnie uslovia dlia kazhdovo vyzova funktsii
	while (i < LEN_PASS) {
		//pochemu, esli napisat srand(time(0)+x) zdes, to programma ne rabotaet?
		switch (rand() % 3) {
		case 0:
			passw[i] = 'a' + rand() % ('z' - 'a' + 1);
			break;
		case 1:
			passw[i] = 'A' + rand() % ('Z' - 'A' + 1);
			break;
		case 2:
			passw[i] = '0' + rand() % ('9' - '0' + 1);
			break;
		}
		i++;
	}
	passw[i] = 0; // chtoby poslednaya stroka byla \0
	puts(passw);
}

int main(){
	int i = 0;
	puts("Your 10 passwords: ");
	while (i<NUM_PASS) {
			getPasswrd(i);
			i++;
	}

	return 0;
}