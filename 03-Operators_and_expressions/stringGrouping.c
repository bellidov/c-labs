#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define LEN 16



int main() {
	int i = 0, count = LEN-1;
	char mystring[LEN + 1], buff;

	
	//generiruem sluchainiy massiv stroku
	srand(time(0));
	while (i < LEN)
		switch (rand() % 2) {
		case 0:
			mystring[i++] = '0' + rand() % ('9' - '0' + 1);
			break;
		case 1:
			mystring[i++] = 'A' + rand() % ('Z' - 'A' + 1);
			break;
		}
	mystring[i] = 0;
	
	printf("sluchainaya stroka sgenerirovana: \n");
	puts(mystring);

	printf("\n");
	
	
	//teper grupiruem
	i = LEN;
	while (i >= 1)
	{
		if (mystring[i-1] >= '0' && mystring[i - 1] <= '9') {
			buff=mystring[count];
			mystring[count] = mystring[i-1];
			mystring[i - 1] = buff;
			count--;
		}
		i--;
	}

	printf("grupirovka proizvedena: \n");
	puts(mystring);

	return 0;
}   