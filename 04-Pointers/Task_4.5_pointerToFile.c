#include <stdio.h>
#include <string.h>
#define N 300

int main() {
	char str[N][N] = { 0 }; 
	char *pstr[N] = { 0 };
	int i = 0, j = 0, len = 0;
	int buff = 0;

	FILE* fp;
	fp = fopen("text.txt", "r+");
	if (fp == NULL) {
		puts("File not found");
		return 1;
	}

	// ���� ����� �� �����
	do {
		fgets(str[len], N, fp);
		pstr[len] = str + len;     // ����������� ���������
		len++;     
	} while (!feof(fp)); 

	//���������� ����������							  -> se puede usar la funcion qsort()
	for (i = 1; i < len; i++)
	for (j = i; j > 0; j--) {
		buff = pstr[j - 1];
		if (strlen(pstr[j]) < strlen(pstr[j - 1])) {
			pstr[j - 1] = pstr[j];
			pstr[j] = buff;
		}
	}

	// ������ ��������������� ����� � ����
	fprintf(fp, "\n\nResults:\n");
	for (i = 0; i < len; i++) {
		fprintf(fp,"%s", pstr[i]);
		if (pstr[i][strlen(pstr[i]) - 1] != '\n')
			fprintf(fp,"%c",'\n');
	}

	fclose(fp);

	return 0;
}


