#include <stdio.h>
#define N 20
#define M 100

int main() {
	char str[N][M] = {0};
	int count = 0, age = 0, buff1 = 0, buff2 = 0, flag = 0;
	char(*young)[M] = { 0 };
	char(*old)[M] = { 0 };

	puts("how many parents you have?");
		scanf("%d", &count);

	puts("Please, enter their names and ages:");
	while (count) {
		scanf("%s - %d", str[count], &age);
		if (flag == 0) {
			buff1 = buff2 = age;
			flag = 1;
		}

		if (age >= buff1){
			old = str[count];
			buff1 = age;
		}
		if(age <= buff2) {
			young = str[count];
			buff2 = age;
		}
		count--;
	}

	printf("The oldest person in your family is %s\nand the youngest is %s\n", old, young);

	return 0;
}