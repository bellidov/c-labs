#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 300
#define M 40

//������������ ����� � ��������� �������

void printWord(char *p) {  
	while (*p && *p != ' ' && *p != '\n')
	putchar(*p++);
}

int getWord(char *str, char *pstr[]) {
	int count = 0, i = 0, inWord = 0;

	while (str[i] != '\n') {
		if (str[i] != ' ' && inWord == 0){
			pstr[count++] = str + i;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return count;
}


int main() {
	char str[N] = { 0 };
	char* pstr[M] = { 0 };
	int count = 0;
	int index = 0;
	char *temp;

	puts("Please, enter your text:");
	fgets(str, N, stdin);
	count = getWord(str, pstr);

	srand(time(0));

	while (count) {
		index = rand() % (count);
	    printWord(pstr[index]);

		if (count > 1)
			putchar(' ');
		else
			putchar('\n');
		
		temp = pstr[index];
		pstr[index] = pstr[count - 1];
		pstr[count - 1] = temp;
		count--;
	}

	return 0;
}