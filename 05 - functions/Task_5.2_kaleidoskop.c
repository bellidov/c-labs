#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 40
#define M 40

void copyMirror(char(*p)[M], int a, int b) {
	int i, j;
	for (i = 0; i < N/2; i++)
	for (j = 0; j < M / 2; j++)
		p[(N - i) + (2 * i - N)*(1 - b)-b][M - j + (2 * j - M)*(1 - a)-a] = p[i][j];
}

void fill(char (*p)[M], char simbol, char simbol2, int a, int b, int r) {
	int i, j;
	for (i = 0; i < a; i++)
	for (j = 0; j <= i; j++) {
		switch (rand() % r) {
		case 0:
			p[i][j] = simbol;
			break;
		case 1:
			p[i][j] = simbol2;
		}
	}

	for (i = 1; i < N / 2; i++)
	for (j = 0; j < i; j++)
		p[j][i] = p[i][j];

}

void draw(char (*p)[M]) {
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < M; j++)
			putchar(p[i][j]);
		putchar('\n');
	}
}

int main() {
	int i = 0, j = 0;
	char iso[N][M] = {0};
	time_t now;

	srand(time(0));

	while (1) {
		fill(iso, ' ', ' ', N, M, 1);		   // ��������� �� ���������
		fill(iso, '.', '@', N/2, M/2, 3);   // ��������� �������� ���� ����� �����������, ������ ����� �������� ��� ����
		
		copyMirror(iso, 1, 0);   // ��������� �� x
		copyMirror(iso, 0, 1);   // ��������� �� y
		copyMirror(iso, 1, 1);   // ��������� �� xy

		system("cls");

		draw(iso);

		now = clock();
		while (clock() < now + 2000);
	}

	return 0;
}
