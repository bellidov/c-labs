#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<locale.h>
#include<time.h>
#define N 900
#define M 500

int getStringsFromDoc(unsigned char (*pstr)[M], FILE* fp){   // destino, source
	int count = 0;
	do {
		fgets(pstr[count], N, fp);
		count++;
	} while (!feof(fp));	
	return count;  // ���������� ���������� ����� � ���������
}

int getWordsFromString(unsigned char *pword[], unsigned char* pstr) {  // destino, source
	int count = 0, inWord = 0, i = 0;
	while (pstr[i] != '\n' && pstr[i] != '\0')
	{
		if (inWord == 0 && ((pstr[i] > 64 && pstr[i]<91) || (pstr[i] > 96 && pstr[i]<123) || (pstr[i] >= 192 && pstr[i] <= 255)))
		{
			pword[count] = pstr + i;
			inWord = 1;
			count++;
		}
		else if (pstr[i] == ' ' || pstr[i] < 65 || (pstr[i] > 90 && pstr[i] < 97) || (pstr[i] > 122 && pstr[i] < 192)){
			inWord = 0;
		}
		i++;
	}
	return count;  // return number od words in string
}

int lenOfWord(unsigned char *pstr){
	int j = 0;

	while (pstr[j] != ' ' && pstr[j] != '\0' && pstr[j] != '\n' && ((pstr[j] > 64 && pstr[j]<91) || (pstr[j] > 96 && pstr[j]<123) || (pstr[j] >= 192 && pstr[j] <= 255))){ // obiazatelno &&
		j++;
	}
	return j;  // return the len of one word
}

void changeWord(unsigned char *p, int lenWord) {
	int index = 0;
	unsigned char temp;
	if (lenWord > 3)  // importante!
	while (lenWord - 2){
		index = rand() % (lenWord - 2) + 1;
		temp = p[index];
		p[index] = p[lenWord - 2];
		p[lenWord - 2] = temp;
		lenWord--;
	}
}

void showStrings(unsigned char (*pstr)[M], int count) {
	int i = 0;
	for (i = 0; i < count; i++)
		printf("%s",pstr[i]);
}

int main() {
	setlocale(LC_ALL, "rus");
	FILE* fp;
	unsigned char str[N][M];   // ������ �� �����
	unsigned char* pwords[M];  // ������ �� ���������� �� ������ ����� � ������
	int countStrings, countWords, i = 0, j = 0, index = 0;
	srand(time(0));
	fp = fopen("text.txt", "rt");
	if (fp == NULL)
	{
		puts("file not found!");
		return 1;
	}
	
	// gets strings from fp to str[][]
	countStrings = getStringsFromDoc(str, fp);
	fclose(fp);


	while (i < countStrings){
		countWords = getWordsFromString(pwords, str[i]); // gets all words from str[][] to pwords[]

		while (j < countWords){
			changeWord(pwords[j], lenOfWord(pwords[j])); // changes each word in array pwords[]
			j++;
		}
		j = 0;
		i++;
	}
	
	showStrings(str, countStrings);
	putchar('\n');
	return 0;
}