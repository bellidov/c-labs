#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>
#define N 300
#define M 1000

int getStringsFromDoc(unsigned char(*pstr)[M], FILE* fp){   // destino, source
	int count = 0;
	do {
		fgets(pstr[count], N, fp);
		count++;
	} while (!feof(fp));
	return count;  // ���������� ���������� ����� � ���������
}

int getWordsFromString(unsigned char *pword[], unsigned char* pstr) {  // destino, source
	int count = 0, inWord = 0, i = 0;
	while (pstr[i] != '\n' && pstr[i] != '\0')
	{
		if (inWord == 0 && ((pstr[i] > 64 && pstr[i]<91) || (pstr[i] > 96 && pstr[i]<123) || (pstr[i] >= 192 && pstr[i] <= 255)))
		{
			pword[count] = pstr + i;
			inWord = 1;
			count++;
		}
		else if (pstr[i] == ' ' || pstr[i] < 65 || (pstr[i] > 90 && pstr[i] < 97) || (pstr[i] > 122 && pstr[i] < 192)){
			inWord = 0;
		}
		i++;
	}
	return count;  // return number of words in string
}

void printWord(unsigned char *p) {
	while (*p && *p != ' ' && *p != '\n')
		putchar(*p++);
}

void changeString(unsigned char *pstr[], int count) {
	int index = 0;
	unsigned char *ptemp = NULL;
	while (count){
		index = rand() % count;
		printWord(pstr[index]);

		if (count > 1)
			putchar(' ');
		else
			putchar('\n');

		ptemp = pstr[index];
		pstr[index] = pstr[count - 1];
		pstr[count - 1] = ptemp;
		count--;
	}
}



void showStrings(unsigned char(*pstr)[M], int count) {
	int i = 0;
	for (i = 0; i < count; i++)
		printf("%s", pstr[i]);
}

int main() {
	setlocale(LC_ALL, "rus");
	FILE* fp;
	unsigned char str[N][M] = { 0 };
	unsigned char *pwords[M] = {NULL};
	int i = 0, countStrings = 0, countWords = 0;
	srand(time(0));
	fp = fopen("text.txt", "rt");
	if (fp == NULL){
		puts("file not found");
		return 1;
	}

	countStrings = getStringsFromDoc(str, fp);

	while (i < countStrings){
		countWords = getWordsFromString(pwords, str[i]);
		changeString(pwords, countWords);
		i++;
	}

	return 0;
}